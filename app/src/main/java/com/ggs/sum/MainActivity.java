package com.ggs.sum;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.jar.JarEntry;

public class MainActivity extends AppCompatActivity {
ImageView  yea, noa, copy, usd, grivna, eu, ru;
EditText chislo;
TextView otvet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


     //   yea=findViewById(R.id.yea);
        noa=findViewById(R.id.noa);
        copy=findViewById(R.id.copy_text);
        usd=findViewById(R.id.usd);
        grivna=findViewById(R.id.grivna);
        eu=findViewById(R.id.eu);
        ru=findViewById(R.id.rubl);

        chislo=findViewById(R.id.chislo);
        otvet=findViewById(R.id.otvet);



        class TheCurrency {
            Integer code;
            TheCurrency(Integer code) {
                this.code = code;
            }
        }


        AmountInWords.setCurrencyMapping(
                new AmountInWords.CurrencyMapping<TheCurrency>() {
                    public AmountInWords.Currency getCurrency(TheCurrency currency) {
                        return AmountInWords.Currency.byCode(currency.code);
                    }
                }
        );



        eu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String enteredText = String.valueOf(chislo.getText());
                enteredText = enteredText.replaceAll(",", ".");
//        fwMoney mo = new fwMoney(enteredText);
//        String money_as_string = mo.num2str();
                //     otvet.setText(money_as_string);
                TheCurrency rub = new TheCurrency(978);

                try {

                    if (enteredText.contains(".")){
                        String tmp = String.valueOf(chislo.getText());
                        int index1 = tmp.indexOf('.');
                        String substr1 = tmp.substring(0,index1);
                        String substr2 = tmp.substring(index1+1, tmp.length());
                        if (substr2.length()>2){
                            Toast.makeText(MainActivity.this, R.string.mnogo, Toast.LENGTH_SHORT).show();
                        } else if (substr2.length()==2){
                            enteredText = enteredText.replaceAll("[,.]", "");
                            Integer      a = Integer.parseInt(enteredText);
                            System.out.println(AmountInWords.format(a, rub));
                            String a2=AmountInWords.format(a, rub);
                            otvet.setText(a2);
                        } else if (substr2.length()==1){
                            enteredText=enteredText+"0";
                            enteredText = enteredText.replaceAll("[,.]", "");
                            Integer      a = Integer.parseInt(enteredText);

                            System.out.println(AmountInWords.format(a, rub));
                            String a1=AmountInWords.format(a, rub);
                            otvet.setText(a1);
                        }

                    }
                    else {
                        enteredText=enteredText+"00";
                        enteredText = enteredText.replaceAll("[,.]", "");
                        Integer      a = Integer.parseInt(enteredText);

                        System.out.println(AmountInWords.format(a, rub));
                        String a3=AmountInWords.format(a, rub);
                        otvet.setText(a3);
                    }




                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


        usd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredText = String.valueOf(chislo.getText());
                enteredText = enteredText.replaceAll(",", ".");
//        fwMoney mo = new fwMoney(enteredText);
//        String money_as_string = mo.num2str();
                //     otvet.setText(money_as_string);
                TheCurrency uah = new TheCurrency(840);

                try {

                    if (enteredText.contains(".")){
                        String tmp = String.valueOf(chislo.getText());
                        int index1 = tmp.indexOf('.');
                        String substr1 = tmp.substring(0,index1);
                        String substr2 = tmp.substring(index1+1, tmp.length());
                        if (substr2.length()>2){
                            Toast.makeText(MainActivity.this, R.string.mnogo, Toast.LENGTH_SHORT).show();
                        } else if (substr2.length()==2){
                            enteredText = enteredText.replaceAll("[,.]", "");
                            Integer      a = Integer.parseInt(enteredText);
                            System.out.println(AmountInWords.format(a, uah));
                            String a2=AmountInWords.format(a, uah);
                            otvet.setText(a2);
                        } else if (substr2.length()==1){
                            enteredText=enteredText+"0";
                            enteredText = enteredText.replaceAll("[,.]", "");
                            Integer      a = Integer.parseInt(enteredText);

                            System.out.println(AmountInWords.format(a, uah));
                            String a1=AmountInWords.format(a, uah);
                            otvet.setText(a1);
                        }

                    }
                    else {
                        enteredText=enteredText+"00";
                        enteredText = enteredText.replaceAll("[,.]", "");
                        Integer      a = Integer.parseInt(enteredText);

                        System.out.println(AmountInWords.format(a, uah));
                        String a3=AmountInWords.format(a, uah);
                        otvet.setText(a3);
                    }




                }catch (Exception e){
                    e.printStackTrace();
                }
            }



        });














        ru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredText = String.valueOf(chislo.getText());
                enteredText = enteredText.replaceAll(",", ".");
//        fwMoney mo = new fwMoney(enteredText);
//        String money_as_string = mo.num2str();
                //     otvet.setText(money_as_string);
                TheCurrency rub = new TheCurrency(643);

                try {

                    if (enteredText.contains(".")){
                        String tmp = String.valueOf(chislo.getText());
                        int index1 = tmp.indexOf('.');
                        String substr1 = tmp.substring(0,index1);
                        String substr2 = tmp.substring(index1+1, tmp.length());
                        if (substr2.length()>2){
                            Toast.makeText(MainActivity.this, R.string.mnogo, Toast.LENGTH_SHORT).show();
                        } else if (substr2.length()==2){
                            enteredText = enteredText.replaceAll("[,.]", "");
                            Integer      a = Integer.parseInt(enteredText);
                            System.out.println(AmountInWords.format(a, rub));
                            String a2=AmountInWords.format(a, rub);
                            otvet.setText(a2);
                        } else if (substr2.length()==1){
                            enteredText=enteredText+"0";
                            enteredText = enteredText.replaceAll("[,.]", "");
                            Integer      a = Integer.parseInt(enteredText);

                            System.out.println(AmountInWords.format(a, rub));
                            String a1=AmountInWords.format(a, rub);
                            otvet.setText(a1);
                        }

                    }
                    else {
                        enteredText=enteredText+"00";
                        enteredText = enteredText.replaceAll("[,.]", "");
                        Integer      a = Integer.parseInt(enteredText);

                        System.out.println(AmountInWords.format(a, rub));
                        String a3=AmountInWords.format(a, rub);
                        otvet.setText(a3);
                    }




                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


grivna.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String enteredText = String.valueOf(chislo.getText());
        enteredText = enteredText.replaceAll(",", ".");
//        fwMoney mo = new fwMoney(enteredText);
//        String money_as_string = mo.num2str();
        //     otvet.setText(money_as_string);
        TheCurrency uah = new TheCurrency(980);

        try {

            if (enteredText.contains(".")){
                String tmp = String.valueOf(chislo.getText());
                int index1 = tmp.indexOf('.');
                String substr1 = tmp.substring(0,index1);
                String substr2 = tmp.substring(index1+1, tmp.length());
                if (substr2.length()>2){
                    Toast.makeText(MainActivity.this, R.string.mnogo, Toast.LENGTH_SHORT).show();
                } else if (substr2.length()==2){
                    enteredText = enteredText.replaceAll("[,.]", "");
                    Integer      a = Integer.parseInt(enteredText);
                    System.out.println(AmountInWords.format(a, uah));
                    String a2=AmountInWords.format(a, uah);
                    otvet.setText(a2);
                } else if (substr2.length()==1){
                    enteredText=enteredText+"0";
                    enteredText = enteredText.replaceAll("[,.]", "");
                    Integer      a = Integer.parseInt(enteredText);

                    System.out.println(AmountInWords.format(a, uah));
                    String a1=AmountInWords.format(a, uah);
                    otvet.setText(a1);
                }

            }
            else {
                enteredText=enteredText+"00";
                enteredText = enteredText.replaceAll("[,.]", "");
                Integer      a = Integer.parseInt(enteredText);

                System.out.println(AmountInWords.format(a, uah));
                String a3=AmountInWords.format(a, uah);
                otvet.setText(a3);
            }




        }catch (Exception e){
            e.printStackTrace();
        }
    }
});


//        yea.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                        String enteredText = String.valueOf(chislo.getText());
//        enteredText = enteredText.replaceAll(",", ".");
////        fwMoney mo = new fwMoney(enteredText);
////        String money_as_string = mo.num2str();
//   //     otvet.setText(money_as_string);
//                TheCurrency uah = new TheCurrency(980);
//
//                try {
//
//                if (enteredText.contains(".")){
//                    String tmp = String.valueOf(chislo.getText());
//                    int index1 = tmp.indexOf('.');
//                    String substr1 = tmp.substring(0,index1);
//                    String substr2 = tmp.substring(index1+1, tmp.length());
//                    if (substr2.length()>2){
//                        Toast.makeText(MainActivity.this, R.string.mnogo, Toast.LENGTH_SHORT).show();
//                    } else if (substr2.length()==2){
//                        enteredText = enteredText.replaceAll("[,.]", "");
//                  Integer      a = Integer.parseInt(enteredText);
//                        System.out.println(AmountInWords.format(a, uah));
//                        String a2=AmountInWords.format(a, uah);
//                        otvet.setText(a2);
//                    } else if (substr2.length()==1){
//                        enteredText=enteredText+"0";
//                        enteredText = enteredText.replaceAll("[,.]", "");
//                        Integer      a = Integer.parseInt(enteredText);
//
//                        System.out.println(AmountInWords.format(a, uah));
//                        String a1=AmountInWords.format(a, uah);
//                        otvet.setText(a1);
//                    }
//
//                }}catch (Exception e){
//                    e.printStackTrace();
//                }
//            }
//        });

        noa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chislo.setText("");
                otvet.setText("");
            }
        });



    }
}